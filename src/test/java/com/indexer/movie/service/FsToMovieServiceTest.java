package com.indexer.movie.service;

import com.indexer.movie.repository.SimpleMovieRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.concurrent.Callable;

import static com.jayway.awaitility.Awaitility.await;
import static java.util.concurrent.TimeUnit.SECONDS;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:/springContext-test.xml")
public class FsToMovieServiceTest {

    private Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired(required = true)
    private FsToMovieService movieService;
    @Autowired(required = true)
    private SimpleMovieRepository movieRepository;

    @Before
    public void emptyData() {
        movieRepository.deleteAll();
    }

    @Test
    public void shouldIndexMultipleMovieEntities() throws Exception {
        // given
        final int expectedMoviesSize = 5;

        // when
        movieService.indexMovies(expectedMoviesSize);

        // then
        await().atMost(20, SECONDS).until(new Callable<Boolean>() {
            public Boolean call() throws Exception {
                int count = (int) movieRepository.count();
                logger.info("Index size: {}", count);

                return count == expectedMoviesSize;
            }
        });
    }
}

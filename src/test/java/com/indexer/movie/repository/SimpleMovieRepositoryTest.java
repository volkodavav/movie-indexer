package com.indexer.movie.repository;

import com.indexer.movie.entity.Movie;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.Collections;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.junit.Assert.assertThat;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:/springContext-test.xml")
public class SimpleMovieRepositoryTest {

    private Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired(required = true)
    private SimpleMovieRepository movieRepository;

    @Before
    public void emptyData() {
        movieRepository.deleteAll();
    }

    @Test
    public void shouldIndexSingleMovieEntity() {

        Movie movie = new Movie();
        movie.setId("123455");
        movie.setTitle("Скорость: Автобус 657 / Heist");
        movie.setUrl("http://tree.tv/film/21811-skorost:-avtobus-657");
        movie.setYear(Collections.singletonList(2015));
        //Indexing using sampleArticleRepository
        movieRepository.save(movie);
        //lets try to search same record in elasticsearch
        Movie indexedMovie = movieRepository.findOne(movie.getId());

        logger.debug("Movie retrieved: {}", movie);

        assertThat(indexedMovie, is(notNullValue()));
        assertThat(indexedMovie.getId(), is(movie.getId()));
    }
}

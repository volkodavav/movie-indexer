package com.indexer.movie.service;

import com.indexer.movie.crawler.FsToUrlCrawler;
import com.indexer.movie.crawler.handler.FsToMoviePublisher;
import org.apache.commons.lang3.Validate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class FsToMovieService {

    private Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired(required = true)
    private FsToUrlCrawler crawler;
    @Autowired(required = true)
    private FsToMoviePublisher publisher;

    public void indexAllMovies() {
        List<String> result = new ArrayList<String>();

        while (crawler.hasNext()) {
            List<String> urls = crawler.next();
            if (urls != null && !urls.isEmpty()) {
                result.addAll(urls);
            }
        }

        publisher.crawl(result);
    }

    public void indexMovies(int maxSize) {
        Validate.isTrue(maxSize > 0, "Size must be > 0");

        List<String> result = new ArrayList<String>();

        int remains = maxSize;
        while (crawler.hasNext()) {
            List<String> urls = crawler.next();
            if (urls != null && !urls.isEmpty()) {
                int numToAdd = Math.min(remains, urls.size());
                List<String> subList = urls.subList(0, numToAdd);
                result.addAll(subList);
                remains -= numToAdd;
            }

            if (remains == 0) {
                // no more items to add
                break;
            }
        }

        logger.debug("URLs to index: {}", result.size());
        publisher.crawl(result);
    }
}

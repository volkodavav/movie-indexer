package com.indexer.movie.runtime;

import net.lightbody.bmp.BrowserMobProxyServer;
import net.lightbody.bmp.client.ClientUtil;
import net.lightbody.bmp.proxy.CaptureType;
import org.apache.commons.lang3.Validate;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.Proxy;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.firefox.FirefoxBinary;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.FactoryBean;
import org.springframework.beans.factory.InitializingBean;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

public class WebDriverProxyFactoryBean implements FactoryBean<WebDriverProxy>, InitializingBean, DisposableBean {

    private final long scriptTimeoutTime = 3;
    private final TimeUnit scriptTimeoutUnit = TimeUnit.SECONDS;
    private final long pageLoadTimeoutTime = 3;
    private final TimeUnit pageLoadTimeoutUnit = TimeUnit.SECONDS;
    private final boolean maximize = true;

    private WebDriverProxy webDriverProxy;

    public WebDriverProxy getObject() throws Exception {
        return webDriverProxy;
    }

    public Class<?> getObjectType() {
        return WebDriverProxy.class;
    }

    public boolean isSingleton() {
        return true;
    }

    public void afterPropertiesSet() throws Exception {
        webDriverProxy = new WebDriverProxy();
        DesiredCapabilities capabilities = buildProxy();
        buildDriver(capabilities);
    }

    public void destroy() throws Exception {
        webDriverProxy.getWebDriver().quit();
        webDriverProxy.getProxy().stop();
    }

    private DesiredCapabilities buildProxy() {
        // start the proxy
        BrowserMobProxyServer proxy = new BrowserMobProxyServer();
        proxy.setHarCaptureTypes(CaptureType.getAllContentCaptureTypes());
        proxy.start(0);

        // get the Selenium proxy object
        Proxy seleniumProxy = ClientUtil.createSeleniumProxy(proxy);

        // configure it as a desired capability
        DesiredCapabilities capabilities = new DesiredCapabilities();
        capabilities.setCapability(CapabilityType.PROXY, seleniumProxy);

        webDriverProxy.setProxy(proxy);
        webDriverProxy.setCapabilities(capabilities);
        return capabilities;
    }

    private void buildDriver(DesiredCapabilities capabilities) {
        final File firebugPath = new File("plugins/firebug-2.0.13-fx.xpi");
        final File firepathPath = new File("plugins/firepath-0.9.7.1-fx.xpi");

        Validate.isTrue(firebugPath.exists(), "Firebug file must exists. Check: " + firebugPath.getAbsolutePath());
        Validate.isTrue(firepathPath.exists(), "Firepath file must exists. Check: " + firepathPath.getAbsolutePath());

        FirefoxProfile profile = new FirefoxProfile();
        try {
            profile.addExtension(firebugPath);
            profile.addExtension(firepathPath);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        CustomFirefoxDriver driver = new CustomFirefoxDriver(new FirefoxBinary(), profile, capabilities);

        driver.manage().timeouts().setScriptTimeout(scriptTimeoutTime, scriptTimeoutUnit);
        driver.manage().timeouts().pageLoadTimeout(pageLoadTimeoutTime, pageLoadTimeoutUnit);
        if (maximize) {
            driver.manage().window().maximize();
        }

        webDriverProxy.setWebDriver(driver);
    }

    private static class CustomFirefoxDriver extends FirefoxDriver {

        private Logger logger = LoggerFactory.getLogger(getClass());

        public CustomFirefoxDriver() {
        }

        public CustomFirefoxDriver(FirefoxProfile profile) {
            super(profile);
        }

        public CustomFirefoxDriver(Capabilities desiredCapabilities) {
            super(desiredCapabilities);
        }

        public CustomFirefoxDriver(Capabilities desiredCapabilities, Capabilities requiredCapabilities) {
            super(desiredCapabilities, requiredCapabilities);
        }

        public CustomFirefoxDriver(FirefoxBinary binary, FirefoxProfile profile) {
            super(binary, profile);
        }

        public CustomFirefoxDriver(FirefoxBinary binary, FirefoxProfile profile, Capabilities capabilities) {
            super(binary, profile, capabilities);
        }

        public CustomFirefoxDriver(FirefoxBinary binary, FirefoxProfile profile, Capabilities desiredCapabilities, Capabilities requiredCapabilities) {
            super(binary, profile, desiredCapabilities, requiredCapabilities);
        }

        @Override
        public void get(String url) {
            try {
                super.get(url);
                this.executeScript("scroll(0, 999999);");
            } catch (TimeoutException e) {
                logger.debug("Timeout error while loading " + url, e);
                this.executeScript("window.stop();");
            }
        }
    }
}

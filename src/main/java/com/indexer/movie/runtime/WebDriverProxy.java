package com.indexer.movie.runtime;

import net.lightbody.bmp.BrowserMobProxy;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;

public class WebDriverProxy {
    private WebDriver webDriver;
    private BrowserMobProxy proxy;
    private DesiredCapabilities capabilities;

    public BrowserMobProxy getProxy() {
        return proxy;
    }

    public void setProxy(BrowserMobProxy proxy) {
        this.proxy = proxy;
    }

    public DesiredCapabilities getCapabilities() {
        return capabilities;
    }

    public void setCapabilities(DesiredCapabilities capabilities) {
        this.capabilities = capabilities;
    }

    public WebDriver getWebDriver() {
        return webDriver;
    }

    public void setWebDriver(WebDriver webDriver) {
        this.webDriver = webDriver;
    }
}

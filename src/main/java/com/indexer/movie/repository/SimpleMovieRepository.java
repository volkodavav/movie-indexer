package com.indexer.movie.repository;

import com.indexer.movie.entity.Movie;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

public interface SimpleMovieRepository extends ElasticsearchRepository<Movie, String> {
}

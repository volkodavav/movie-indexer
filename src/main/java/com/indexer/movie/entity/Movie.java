package com.indexer.movie.entity;

import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldIndex;
import org.springframework.data.elasticsearch.annotations.FieldType;

import java.util.List;

import static org.springframework.data.elasticsearch.annotations.FieldType.Integer;
import static org.springframework.data.elasticsearch.annotations.FieldType.String;

@Document(indexName = "movies", type = "movie")
public class Movie {

    @Id
    private String id;
    private String url;
    @Field(
            type = FieldType.String,
            index = FieldIndex.analyzed,
            searchAnalyzer = "russian",
            indexAnalyzer = "russian",
            store = true
    )
    private String title;
    @Field(type = Integer, store = true)
    private List<Integer> year;
    @Field(
            type = FieldType.String,
            index = FieldIndex.analyzed,
            searchAnalyzer = "russian",
            indexAnalyzer = "russian",
            store = true
    )
    private String info;
    private double posScore;
    private double negScore;
    private int commentsSize;
    @Field(type = String, store = true)
    private List<String> genreList;
    @Field(type = String, store = true)
    private List<String> directorList;
    @Field(type = String, store = true)
    private List<String> actorList;
    @Field(type = String, store = true)
    private List<String> languageList;
    @Field(type = String, store = true)
    private List<String> countryList;
    private String posterUrl;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<Integer> getYear() {
        return year;
    }

    public void setYear(List<Integer> year) {
        this.year = year;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public double getPosScore() {
        return posScore;
    }

    public void setPosScore(double posScore) {
        this.posScore = posScore;
    }

    public double getNegScore() {
        return negScore;
    }

    public void setNegScore(double negScore) {
        this.negScore = negScore;
    }

    public int getCommentsSize() {
        return commentsSize;
    }

    public void setCommentsSize(int commentsSize) {
        this.commentsSize = commentsSize;
    }

    public List<String> getGenreList() {
        return genreList;
    }

    public void setGenreList(List<String> genreList) {
        this.genreList = genreList;
    }

    public List<String> getDirectorList() {
        return directorList;
    }

    public void setDirectorList(List<String> directorList) {
        this.directorList = directorList;
    }

    public List<String> getActorList() {
        return actorList;
    }

    public void setActorList(List<String> actorList) {
        this.actorList = actorList;
    }

    public List<String> getLanguageList() {
        return languageList;
    }

    public void setLanguageList(List<String> languageList) {
        this.languageList = languageList;
    }

    public List<String> getCountryList() {
        return countryList;
    }

    public void setCountryList(List<String> countryList) {
        this.countryList = countryList;
    }

    public String getPosterUrl() {
        return posterUrl;
    }

    public void setPosterUrl(String posterUrl) {
        this.posterUrl = posterUrl;
    }

    @Override
    public java.lang.String toString() {
        final StringBuilder sb = new StringBuilder("Movie{");
        sb.append("id='").append(id).append('\'');
        sb.append(", url='").append(url).append('\'');
        sb.append(", title='").append(title).append('\'');
        sb.append(", year=").append(year);
        sb.append(", info='").append(info).append('\'');
        sb.append(", posScore=").append(posScore);
        sb.append(", negScore=").append(negScore);
        sb.append(", commentsSize=").append(commentsSize);
        sb.append(", genreList=").append(genreList);
        sb.append(", directorList=").append(directorList);
        sb.append(", actorList=").append(actorList);
        sb.append(", languageList=").append(languageList);
        sb.append(", countryList=").append(countryList);
        sb.append(", posterUrl='").append(posterUrl).append('\'');
        sb.append('}');
        return sb.toString();
    }
}

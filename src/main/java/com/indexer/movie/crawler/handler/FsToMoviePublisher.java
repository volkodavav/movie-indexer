package com.indexer.movie.crawler.handler;

import com.google.common.eventbus.AsyncEventBus;
import com.indexer.movie.converter.FsToConverter;
import com.indexer.movie.entity.Movie;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class FsToMoviePublisher {

    private Logger logger = LoggerFactory.getLogger(getClass());
    @Autowired(required = true)
    private FsToConverter converter;
    @Autowired(required = true)
    private AsyncEventBus eventBus;

    public void crawl(List<String> urls) {
        List<Movie> movies = new ArrayList<Movie>();

        for (String url : urls) {
            try {
                Movie movie = converter.convert(url);
                logger.debug("Movie: {}", movie);
                movies.add(movie);
            } catch (Exception e) {
                logger.error("Can't get {}", url, e);
            }
        }

        logger.debug("Publish: {}", movies.size());
        eventBus.post(movies);
    }
}

package com.indexer.movie.crawler.handler;

import com.google.common.eventbus.AllowConcurrentEvents;
import com.google.common.eventbus.AsyncEventBus;
import com.google.common.eventbus.Subscribe;
import com.indexer.movie.entity.Movie;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.util.List;

@Component
public class MovieSubscriber {

    private Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired(required = true)
    private ElasticsearchRepository<Movie, String> movieRepository;
    @Autowired(required = true)
    private AsyncEventBus eventBus;

    @PostConstruct
    public void postConstruct() {
        eventBus.register(this);
    }

    @PreDestroy
    public void preDestroy() {
        eventBus.unregister(this);
    }

    @Subscribe
    @AllowConcurrentEvents
    public void handleMovies(List<Movie> movies) {
        logger.debug("Handle: {}", movies.size());
        movieRepository.save(movies);
        logger.debug("Movies stored: {}", movies);
    }
}

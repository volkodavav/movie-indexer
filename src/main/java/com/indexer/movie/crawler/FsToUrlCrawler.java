package com.indexer.movie.crawler;

import com.indexer.movie.config.AppConfig;
import com.indexer.movie.runtime.WebDriverProxy;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

@Component
public class FsToUrlCrawler implements Iterator<List<String>> {

    private Logger logger = LoggerFactory.getLogger(getClass());
    @Autowired(required = true)
    private WebDriverProxy webDriverProxy;

    private int pageNum = 0;
    private String urlPattern = "http://fs.to/video/films/?page=%s";

    public boolean hasNext() {
        String url = getPageUrl(pageNum + 1);
        logger.debug("Check URL: {}", url);
        try {
            webDriverProxy.getWebDriver().get(url);
            WebElement element = (new WebDriverWait(webDriverProxy.getWebDriver(), AppConfig.ELEMENT_TIMEOUT_IN_SECONDS))
                    .until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(".b-section-list.no-pager>div>span")));
            logger.debug("Next link: {}", element);
            if (element != null) {
                return false;
            }
        } catch (NoSuchElementException e) {
            // nothing to do
        } catch (TimeoutException e) {
            // nothing to do
        } catch (Exception e) {
            logger.error("Can't get {}", url, e);
        }

        return true;
    }

    public List<String> next() {
        List<String> urls = new ArrayList<String>();
        String url = getPageUrl(pageNum);
        logger.debug("Next URL: {}", url);
        try {
            webDriverProxy.getWebDriver().get(url);
            List<WebElement> items = (new WebDriverWait(webDriverProxy.getWebDriver(), AppConfig.ELEMENT_TIMEOUT_IN_SECONDS))
                    .until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.cssSelector(".b-poster-tile__link")));
            logger.debug("Items: {}", items);

            for (WebElement item : items) {
                String href = item.getAttribute("href");
                urls.add(href);
            }
        } catch (NoSuchElementException e) {
            // nothing to do
        } catch (TimeoutException e) {
            // nothing to do
        } catch (Exception e) {
            logger.error("Can't get {}", url, e);
        }

        pageNum++;
        logger.debug("URLs: {}", urls);

        return urls;
    }

    private String getPageUrl(int pageNum) {
        return String.format(urlPattern, pageNum);
    }

    @PostConstruct
    public void postInit() {
        logger.info("Post initialize FS.TO crawler");
        List<String> allowUrlPatterns = new ArrayList<String>();
        allowUrlPatterns.add("https?://([a-z0-9]*\\.)?fs\\.to/.*");
        allowUrlPatterns.add("https?://([a-z0-9]*\\.)?dotua\\.org/.*");

        // All the URLs that are not from our sites are blocked and a status code of 404 is returned
        webDriverProxy.getProxy().whitelistRequests(allowUrlPatterns, 404);
    }
}

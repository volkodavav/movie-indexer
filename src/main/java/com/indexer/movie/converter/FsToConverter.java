package com.indexer.movie.converter;

import com.google.common.base.Function;
import com.google.common.collect.Lists;
import com.indexer.movie.config.AppConfig;
import com.indexer.movie.entity.Movie;
import com.indexer.movie.runtime.WebDriverProxy;
import com.jayway.jsonpath.JsonPath;
import com.jayway.jsonpath.ReadContext;
import net.lightbody.bmp.core.har.*;
import org.apache.commons.lang3.StringUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class FsToConverter {

    private Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired(required = true)
    private WebDriverProxy webDriverProxy;

    public Movie convert(String originalUrl) {

        Movie movie = new Movie();

        webDriverProxy.getProxy().newHar();
        webDriverProxy.getWebDriver().get(originalUrl);
        Har har = webDriverProxy.getProxy().getHar();

        boolean available = infoAvailable();
        if (available == false) {
            throw new IllegalStateException("Movie not available: " + originalUrl);
        }

        updateMovie(movie, har);
        movie.setUrl(originalUrl);
        movie.setPosScore(getLocalScore());
        movie.setNegScore(getLocalNegativeScore());
        movie.setCommentsSize(getCommentsCount());

        return movie;
    }

    private void updateMovie(Movie movie, Har har) {
        if (har == null) {
            throw new IllegalStateException("Proxy data must not be null");
        }

        HarLog log = har.getLog();
        if (log == null) {
            throw new IllegalStateException("Proxy log must not be null");
        }

        List<HarEntry> entries = log.getEntries();
        if (entries == null) {
            throw new IllegalStateException("Proxy entries must not be null");
        }

        for (HarEntry harEntry : entries) {
            HarRequest request = harEntry.getRequest();
            String url = request.getUrl();
            if (url.startsWith("http://fs.to/video/films/view_iframe") &&
                    url.endsWith("isStartRequest=true")) {
                HarResponse response = harEntry.getResponse();
                if (response == null) {
                    continue;
                }

                HarContent content = response.getContent();
                if (content == null) {
                    continue;
                }

                String jsonText = content.getText();
                if (jsonText == null) {
                    continue;
                }

                updateMovie(movie, jsonText);
                // exit loop
                break;
            }
        }
    }

    private void updateMovie(Movie movie, String jsonText) {
        if (StringUtils.isBlank(jsonText)) {
            throw new IllegalStateException("Response content must not be null or empty");
        }

        logger.trace("Response: {}", jsonText);

        ReadContext ctx = JsonPath.parse(jsonText);
        String description = ctx.read("$.player.tags.description");
        String posterUrl = ctx.read("$.player.tags.image");
        String title = ctx.read("$.coverData.title");
        String titleOrigin = ctx.read("$.coverData.title_origin");
        List<String> genre = ctx.read("$.coverData.genre.*.title");
        List<String> yearAsString = ctx.read("$.coverData.year.*.title");
        List<Integer> year = Lists.transform(yearAsString, new Function<String, Integer>() {
            public Integer apply(String input) {
                return Integer.parseInt(input);
            }
        });
        List<String> country = ctx.read("$.coverData.made_in.*.title");
        List<String> director = ctx.read("$.coverData.director.*.title");
        List<String> actor = ctx.read("$.coverData.cast.*.title");
        List<String> languages = ctx.read("$.actionsData.languages[*].flag");

        movie.setTitle(title + " / " + titleOrigin);
        movie.setInfo(description);
        movie.setPosterUrl(posterUrl);
        movie.setYear(year);
        movie.setCountryList(country);
        movie.setGenreList(genre);
        movie.setActorList(actor);
        movie.setDirectorList(director);
        movie.setLanguageList(languages);
    }

    private boolean infoAvailable() {
        WebElement element = findElementByCssSelector(".b-video-error__title");
        return element == null;
    }

    private double getLocalScore() {
        WebElement element = findElementByCssSelector(".b-tab-item__vote-value.m-tab-item__vote-value_type_yes");
        if (element != null) {
            String text = element.getText();
            if (StringUtils.isNotBlank(text)) {
                return Double.parseDouble(text.trim());
            }
        }

        return 0;
    }

    private double getLocalNegativeScore() {
        WebElement element = findElementByCssSelector(".b-tab-item__vote-value.m-tab-item__vote-value_type_no");
        if (element != null) {
            String text = element.getText();
            if (StringUtils.isNotBlank(text)) {
                return Double.parseDouble(text.trim());
            }
        }

        return 0;
    }

    private int getCommentsCount() {
        WebElement element = findElementByCssSelector(".b-item-material-comments__count");
        if (element != null) {
            String text = element.getText();
            if (StringUtils.isNotBlank(text)) {
                return Integer.parseInt(text.trim().split("\\s+")[0]);
            }
        }

        return 0;
    }

    private WebElement findElementByCssSelector(String cssSelector) {
        WebElement found = null;
        try {
            found = (new WebDriverWait(webDriverProxy.getWebDriver(), AppConfig.ELEMENT_TIMEOUT_IN_SECONDS))
                    .until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(cssSelector)));
        } catch (NoSuchElementException e) {
            // nothing to do
        } catch (TimeoutException e) {
            // nothing to do
        } catch (Exception e) {
            logger.error("[ " + webDriverProxy.getWebDriver().getCurrentUrl() + " ] Can't get element by " + cssSelector, e);
        }

        return found;
    }
}
